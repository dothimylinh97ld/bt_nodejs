var mongoose = require("mongoose");
var userScheme = mongoose.Schema({
    username: {
        type: String,
        require:true
    },
    password: {
        type: String,
        require:true
    },
    firstname: {
        type: String,
        require:true
    },
    lastname: {
        type: String,
        require:true
    }
})
var User = module.exports = mongoose.model('User', userScheme);