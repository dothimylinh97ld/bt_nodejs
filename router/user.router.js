var express = require("express");
var router = express.Router();
var userController = require("../controller/userController");

module.exports = function(){
    router.post('/', userController.register);
    return router;
}