var express = require("express");
var router = express.Router();
var userController = require('../controller/userController'); // gọi login từ userController

module.exports = {
    login : login
}

// tạo thêm đường dẫn sau /
function login(){
    router.post('/', userController.login);
    return router;
}